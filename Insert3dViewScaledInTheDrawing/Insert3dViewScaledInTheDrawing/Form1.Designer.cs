﻿namespace Insert3dViewScaledInTheDrawing
{
    partial class Insert3dViewScaledInTheDrawing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Insert3dViewScaledInTheDrawing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 217);
            this.Name = "Insert3dViewScaledInTheDrawing";
            this.Text = "Insert3dViewScaledInTheDrawing";
            this.Load += new System.EventHandler(this.Insert3dViewScaledInTheDrawing_Load);
            this.ResumeLayout(false);

        }

        #endregion
    }
}

