﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Globalization;
using System.Runtime.Remoting;
using Tekla.Structures.Internal;
using System.Diagnostics;

using System.Runtime.Serialization;
using System.Xml.Serialization;

using System.IO;
using System.Collections;

//Tekla:
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
using TSD = Tekla.Structures.Drawing;
using TSDUI = Tekla.Structures.Drawing.UI;
using System.Threading;

using TeklaMacroBuilder;

//using System.Collections;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

namespace Insert3dViewScaledInTheDrawing
{
    public partial class Insert3dViewScaledInTheDrawing : Form
    {
        public Insert3dViewScaledInTheDrawing()
        {
            InitializeComponent();
            this.Visible = false;
        }

        private void Insert3dViewScaledInTheDrawing_Load(object sender, EventArgs e)
        {
            Insert3dViewInDrawing();
        }

        // PUBLIC VARS:
        string appName = "Insert3dViewScaledInTheDrawing";
        public TSM.Model model = new TSM.Model();
        public static TSD.DrawingHandler drawingHandler = new TSD.DrawingHandler();
        public TSD.Drawing drawing = drawingHandler.GetActiveDrawing();
        public T3D.Point point1 = null;
        public T3D.Point point2 = null;
        public TSD.ViewBase view = null;

        /// <summary>
        /// Start Automaticly on run
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Insert3dView_Click(object sender, EventArgs e)
        {
            Insert3dViewInDrawing();
        }

        /// <summary>
        /// Main program functions
        /// </summary>
        private void Insert3dViewInDrawing()
        {
            try
            {
                                                                                            // Save current user View Properties and after the 3d view creation load it back
                                                                                            /*
                                                                                            
                                                                                            */
				// Enable SnapToAnyPosition mode:
				EnableSnapToAnyPosition();

				// Ask user to select the object view space:
				SelectNewViewSpace();

				// Disable SnapToAnyPosition mode:
				DisableSnapToAnyPosition();

				// Insert the 3D view with a prerecorded macro:
				Insert3dView();

				// Modify 3D view settings to fit the needs:
				Modify3dViewSettings();

                // Move created View to the selected place:
                MoveView(point1, point2);

                // Exit the program:
                ExitTheProgram();
            }
            catch (Exception ex) { Debug.Print(ex.Message); TSO.Operation.DisplayPrompt(ex.Message.ToString()); ExitTheProgram(); }
        }

        /// <summary>
        /// Select the bounding box
        /// </summary>
        private void SelectNewViewSpace()
        {
            TSD.DrawingHandler drawingHandler = new TSD.DrawingHandler();
            TSDUI.Picker picker = drawingHandler.GetPicker();
            picker.PickTwoPoints(appName + ": Pick 1st point", appName + ": Pick 2nd point", out point1, out point2, out view);
        }

        /// <summary>
        /// Modify created view settings
        /// </summary>
        private void Modify3dViewSettings()
        {
            drawing = drawingHandler.GetActiveDrawing();
            TSD.DrawingObjectEnumerator doe = drawing.GetSheet().GetViews();

            float widthScale100 = 0;
            float widthScale1 = 0;
            float heightScale100 = 0;
            float heightScale1 = 0;
            float bigScale = 150;
            //float smallScale = 1;

            // Itterate through all the views:
            foreach (TSD.View view in doe)
            {
                // Detect the 3d view by unique properties:                         // MIGHT NOT BE THE BEST !!!
                //if (view.Attributes.TagsAttributes.TagA1.TagContent.GetUnformattedString() == "{\n  | - |  }\n")  // This worked for TS21
                if (view.ViewType.ToString() == "_3DView")
                {
                    view.Select();

                    float pointDeltaX = (float)Math.Abs(point2.X - point1.X);
                    float pointDeltaY = (float)Math.Abs(point2.Y - point1.Y);

                    // Load Saveas Properties:

                    // For Normal situations:
                    view.Attributes.LoadAttributes("A_3D view");

                    //view.Attributes.LoadAttributes("3D view");
                    view.Name = "_3D_";

                    //
                    // Calculate the linear equation for the view scaling any apply it based on the needs:
                    //

                    view.Attributes.Scale = bigScale;
                    view.Modify();

                    // Get Width at that scale 100:
                    widthScale100 = (float)view.Width;
                    heightScale100 = (float)view.Height;

                    // Get Width at scale 1:
                    view.Attributes.Scale = 1;
                    view.Modify();
                    widthScale1 = (float)view.Width;
                    heightScale1 = (float)view.Height;

                    // Get smallest selected frame edge:
                    if (pointDeltaX < pointDeltaY)
                    {
                        float selected_minEdge = pointDeltaX;
                        float selectedLength1 = widthScale1;
                        float selectedLength100 = widthScale100;

                        float m = (((1 / 1) - (1 / bigScale)) / (selectedLength1 - selectedLength100));
                        float b = (1 / 1) - (m * selectedLength1);
                        float x = selected_minEdge;
                        float y = m * selected_minEdge + b;
                        double y1 = Math.Pow(y, -1);
                        view.Attributes.Scale = y1 + 5 / 100 * y1;
                        view.Modify();
                    }
                    else
                    {
                        float selected_minEdge = pointDeltaY;
                        float selectedLength1 = heightScale1;
                        float selectedLength100 = heightScale100;

                        float m = (((1 / 1) - (1 / bigScale)) / (selectedLength1 - selectedLength100));
                        float b = (1 / 1) - (m * selectedLength1);
                        float x = selected_minEdge;
                        float y = m * selected_minEdge + b;
                        double y1 = Math.Pow(y, -1);
                        view.Attributes.Scale = y1 + 5 / 100 * y1;
                        view.Modify();
                    }
                }
            }
        }

        /// <summary>
        /// Move the view to correct place
        /// </summary>
        /// <param name="point"></param>
        private void MoveView(T3D.Point point1, T3D.Point point2)
        {
            drawing = drawingHandler.GetActiveDrawing();
            TSD.DrawingObjectEnumerator doe = drawing.GetSheet().GetViews();
            T3D.Point point = new T3D.Point();

            // Determine the upper left point:
            if (point1.Y > point2.Y)
            { point.Y = point1.Y; }
            else { point.Y = point2.Y; }
            if (point1.X < point2.X)
            { point.X = point1.X; }
            else { point.X = point2.X; }

            // Find the created view and move it:
            foreach (TSD.View view in doe)
            {
                if (view.Name == "_3D_")
                {
                    view.Origin.Translate(
                        point.X + view.Width / 2,
                        point.Y - view.Height / 2,
                        0);

                    view.Modify();
                    return;
                }
            }
        }

        private void ExitTheProgram()
        {
			DisableSnapToAnyPosition();
			Environment.Exit(1);
        }

		private void EnableSnapToAnyPosition()
		{
			MacroBuilder macroBuilder = new MacroBuilder();
			macroBuilder.ValueChange("main_frame", "gr_free_tb", "1");
			macroBuilder.Run();
		}

		private void DisableSnapToAnyPosition()
		{
			MacroBuilder macroBuilder = new MacroBuilder();
			macroBuilder.ValueChange("main_frame", "gr_free_tb", "0");
			macroBuilder.Run();
		}

		private void Insert3dView()
		{
			MacroBuilder macroBuilder = new MacroBuilder();
			macroBuilder.CommandEnd();
			macroBuilder.CommandStart("ailCreate3dView", "", "main_frame");
			macroBuilder.Run();
		}
	}
}
